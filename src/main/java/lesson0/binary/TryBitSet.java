package lesson0.binary;

public class TryBitSet {

   public static void main(String[] args) {
      BitSet b = new BitSet();

      System.out.println(b);
      System.out.println(b.get(4));

      b.set(true, 4);
      System.out.println(b);
      System.out.println(b.get(4));

      b.set(false,4);
      System.out.println(b);
      System.out.println(b.get(4));

      b.flip(4);
      System.out.println(b);
      System.out.println(b.get(4));
   }
}
