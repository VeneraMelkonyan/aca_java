package homeworks.vachagan_balayan.lesson1.roulette;

import lesson1._roulette.interfaces.IDealer;
import lesson1._roulette.interfaces.IPlayer;
import lesson1._roulette.interfaces.IRoulette;
import lesson1._roulette.interfaces.IStrategy;

public class Main {

   public static void main(String[] args) {
      final int startCash = 1000;
      final int numberOfGames = 10;

      IRoulette roulette = new Roulette();
      IDealer dealer = new Dealer(roulette);

      for (int i = 0; i < numberOfGames; i++) {
         IStrategy strategy = new DoubleOnLossStrategy();
         IPlayer player = new Player(startCash, strategy);

         int maxCash = 0;
         int numberOfBets = 0;
         while (player.hasMoney()) {
            player.playWith(dealer);
            maxCash = Math.max(maxCash, player.getCash());
            numberOfBets++;
         }

         System.out.println("max cash ever  : " + maxCash);
         System.out.println("number of bets : " + numberOfBets);
         System.out.println();
      }

   }
}
