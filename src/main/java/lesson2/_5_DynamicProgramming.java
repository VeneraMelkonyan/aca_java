package lesson2;

import java.util.HashMap;
import java.util.Map;

// time is O(n) linear, space is O(n) since we have cache
class FibRecursive {
   boolean showRecursion = true;
   int depth = 0;

   Map<Integer, Integer> cache = new HashMap<>();


   int fib(int n) {

      Integer cached = cache.get(n);

      if (cached != null) {
         for (int i = 0; i < depth; i++) {
            System.out.print('\t');
         }

         System.out.println(">> fib(" + n + ") // cached = " + cached);
         return cached;
      }

      if (showRecursion) {
         for (int i = 0; i < depth; i++) {
            System.out.print('\t');
         }
         System.out.println(">> fib(" + n + ")");
      }

      int result;
      if (n <= 1) {
         result = n;
      } else {
         depth++;
         result = fib(n - 1) + fib(n - 2);
         depth--;
      }


      if (showRecursion) {
         for (int i = 0; i < depth; i++) {
            System.out.print('\t');
         }
         System.out.println("<< " + result);
      }

      cache.put(n, result);

      return result;
   }
}

public class _5_DynamicProgramming {

   public static void main(String[] args) {
      System.out.println(new FibRecursive().fib(10));
   }
}
