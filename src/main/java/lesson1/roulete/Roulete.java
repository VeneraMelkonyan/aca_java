package lesson1.roulete;

public class Roulete {

   public static void main(String[] args) {
      Player player = new Player(1000);
      String playerBet = player.bet();
      Board board = new Board();

      int amount = 1;
      int numOfGames = 0;
      int maxAmount = 0;

      while (player.getBudget() > 0) {
         board.generate();
         if (playerBet.equals(board.getColor())) {
            player.addToBudget(amount);
            System.out.println("\nPlayer Won -- " + player.getBudget());

            if (maxAmount < player.getBudget()) {
               maxAmount = player.getBudget();
            }

         } else {
            int something = 1;
            player.removeFromBudget(amount);
            amount *= 2;

            if (playerBet.equals("Black"))
               playerBet = "Red";
            else {
               playerBet = "Black";
            }


            System.out.println("\nPlayer Lost -- " + player.getBudget());
         }
         ++numOfGames;
      }

      System.out.println("Player played  " + numOfGames + " times");
      System.out.println("Max amount :" + maxAmount);
   }
}



