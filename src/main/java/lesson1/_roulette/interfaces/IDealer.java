package lesson1._roulette.interfaces;

public interface IDealer {

   void takeBet(IPlayer player, IRoulette.Color color, int amount);

}
